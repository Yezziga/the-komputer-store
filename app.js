const LAPTOPSELECTOR = document.getElementById("laptops")
const PAYBALANCETXT = document.getElementById("txt-pay-balance")
const BANKBALANCETXT = document.getElementById("txt-bank-balance")
const FEATURESLIST = document.getElementById("list-features")
const LAPTOPNAME = document.getElementById("laptop-name")
const DESCRIPTION = document.getElementById("description")
const PRICE = document.getElementById("price")
const IMAGE = document.getElementById("image")
const imageURLs = ["https://hips.hearstapps.com/pop.h-cdn.co/assets/16/17/1461938344-lte.jpg", 
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUSEhIVFhUVFxcXFRUXGBgXGBUVFxgXFxUWFhUYHSkgGBolHRcWITEhJSkrLi4vFx8zODMsNygtLisBCgoKDg0OGhAQGy0mICUtLS0tLS0tLS0tMjAtLS0rLS0tNTArLS4tLSstLS0vLTAyLy0tLS8vLS0tLS0tLS0tLf/AABEIAKYBMAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABQMEBgIBB//EAE8QAAEDAQUCCQcHCQYGAwEAAAEAAhEDBAUSITFBUQYiMmFxcoGRsRNCobLB0fAHFCMzUmLCQ1NUc4KSorPhFRYkg5PSNDVEY+Lxo8PTJf/EABkBAQADAQEAAAAAAAAAAAAAAAABAgMEBf/EAC0RAAICAQQABAQGAwAAAAAAAAABAgMRBBIhMUFRYZEFMoGxExQicaHwI0Lh/9oADAMBAAIRAxEAPwD7ihCEAIQhACEIQAhCzF8W6oKz2h5AbAABjVoPtUN4Bp14XAalYl1oedXuPSSuJVd4No61MGr294UTrypD8o3vnwWRQo3g1Tr4ojzp7D7lG6/KX3j2f1WaQm5kmhdf7NjHehRuv/dT9P8ARIwvVG5gcG/nbGDvK4dfdTc0dh96VBdYTuTcwXze9XeB2Bcm8qv2z6FQc8DUgdJhV6t6UG8qvRb1qjB4lMsDT59UPnu71ybQ8+e7vKRHhPYR/wBbZuytTPg5TUuEFlc5rW12EvIa2JMuOQEgQnIGvlDvPepal51KbjBkQDBz1aCfSoFzbBxuxvqhEDRXXerK0gZPGrfaN4U9ot1NhwudBiYz00lfP3VXMqYmkgiII6AnFqtRqmm86mkJ6Q949itu4INBXvqiyJccxOQOh0VV3CalsDz2Ae1IrxoFxbDgBgb5snTfPsS6tYCBJr1gPuMpu9HknFRuZJqzwlGyme/+i8/t9x0YO9Y406XnWiuek+T9VjVXdQs08q0OO755V9X5wPBMsG6F81D5rfSpBeT+jsWF8nZv0eq7rB9T8TpXIsdmmW2GiTvNEz3+RKcg3wvPfUaOktCpXpfbW06hbXZiDHkQ9syGkjKVnbDhxYfm7KcRo2NcUQCxv2T6Fbtv1VSPsO9UqMgZ2a21DJL3ZMeddoYSDHSmtyXsK7YOVQcob/vN5vBIrJt/Vv8A5bkmoVXMcHNMEZgqU8A+lIS25b2bXbnk8cpv4hzeCZLQgEIQgBCEIAQhCAFj75+vqdLfUatgshfP19Tpb6jVWfQFd4VqlJrXiz1arXbaZo5HPUVKrTsOxUm3rVIBFkeJ+3UpD1HOWgvLKyMP3vRL59ErPstEw0NOQzkjs25bFtVVCUcyMLbnGWAZbrUT9RQaNZNoeTHQKAHpUr3WmMnWdszq2pU010c3euKTzIB2giPHNWaT5B5iPSIJW/5aC5wZq9vxKzvnQEurUT1KDmx+9VcoX+XP/Uvb1WUR69NyY06uoP8A7+JXDqeZ3bOjZnsXRXp6vGJ52p1F0XmMuCiynV860ViBqfoh6lNqs0boLxL61f8AZr1WHf5jhCmwnm9C5vK2tpUn1nCcIJA3nYMzBVrKq4riK9jKrUWyfMn74KbrsaDGKo4feq1X+s8qF9z0HcujTd1mh3rApzdlsD2MeIwvzBbBExmARrBnuXNc5kZK8FB/6oyslYlne/dilly2ZnIs9FvRTYPALi8KTWNBaGt/ZaPYnDGA7f8A3pCzHDp7gKVJoEvc6dkgYcu8+hLbI1wcvI10DzenPlc+vgyFl8uB4uMgTLgIHF3TtU7LxdUNIGoXD5zZuKSXR9I06k5aj4Cz1WzsIw43Y2Q2MM5S482/TnG4q1czMNVjXCHC02XSeMC8unmERGW3mK8mOtlY3FpdP7HuOaxhRS5XSS8UfUAvbYPBvqheLq3eweAXObCCvyj8bE0pcil+rP8ANqJVaOUfjYrVgqknCTk1ojmlziVYDO0eb1G+CjEQZEgiCDuII9q7tUy2B5jdvN0KHPm8VVg5NJg0pjsDf6LvyU/lC0bAAMu0gryDvHd/VEHeO7+qA9+bj89V7qf/AOa9wvmPJu6eJHTrK5w859HuXsHefR7kB7UokEGIzzOWkOjTp9Kitv1b+o7wK7w9Pf7lFbGDyb9eQ7adx50Bbsf4H/y3JJKd2LZ1Hfy3JIrAls9ZzHB7TDhofjYtxc15iuyYhzYDhznQjmMFYMLScCznV/Y/GpXYNQhCFcgEIQgBCEIAWPvn6+p0t9Ri2Cx98/X1OlvqNVZ9Eoj4QsJsFOJ+sEwJyl85ensCytmLBJBc3POC0DbnlzR38y2F7n/BUoMHymX/AMhWWa0OkOJOExsBA35Lt0+78Phf33R52qzu4Z1RaC6QZ2gkznulXLvAxNa7MEgGO7L0pNWwtMsfzGJnp1UlCs4Zh47V6DqcoeXH98zgV22XKH97saynXa1rmva2lUDiQeISGGPsmQ6ensFSzWIvpMf85LS4OJxEDPMNAG7EDn0JfVtDg20PxNc40neUl5cWt5Wk8XPxO9K7la17QXDlNBnn7FxaOmcE4OeX3nvwSf8AJvqLYfM4ce3qaOldwDi19syc10PDsID2mmIzdDhD3bRySorXddAtIrWoPBg4CeUC0ECC7KQ4ZncVHSsjIAwgpFwws1QUw6iHDCZfhgQ0AyXHWOieyM+m2Moxby39Ec9Wog5KKjj6nNOo4tDMRwiAGyYGwQNAtbVqSeYadCTXDd+CkzykOcQDxgC5pOZGLaB3p1HOtU8pPBy3WbuEcx4hJeEVFr6lMudhAFQyZ+7nl8ZpmypNRzdwGXPv9K6fSONrp4oa4FsDPFEGYnKNBAOIzoFjqqfxK3Dzx9y+klssUn1z9jLX/YcJxtOchxnTfIzVe6pdVpPc7jG0WSRA42J9RwOsmBh7wtHbqGZfidGXFBgEZADIZHnzVKpYsD6BER86oxHWAJMnXKcvtHYvOXw51J2Z8/t+56sNXCc1FLtr+9G3ReB8G+AQuLxOfY31QuQ9MR1+UfjYp7s5Tuq31nKCvyj8bFLdfLf1W+LlYDm0eb1G+1RKS0at6jfaolUHqF4hAeoXiEB6orX9W/qu8CpFHaRxHdV3gUBZu7Z1HfyykoKc3d5vUPqFJQrA9Wk4F61f2PxrNhaTgZrV6GfiUrsGoQhCuQCEIQAhCEALHXv9fU6w9Vq2Kxt6/XVOt7Aqz6JQs4f2p1O7aDmGHeXEfu1lg6F/tIJqucDuY2S7nzIA7+xbj5Ro/syiDP1wjpirrvylfJXELKOrton+l8Y6OHUVqbwzQ/25Q3VR+yw+jGITq5QLQ5gp4pfJGIAYWNJBcQHGcwcubnC+e2h2ESvpPAAQ+u8/k8FAf5bQH95DSuiXxC+yO3PfuTpNJVuc2s4X8+A6vW5KNBra1Cnhq03tLqjQG4mvcG1A+IxCHF2hzAS6vZ2SHMhgI5A0BzJgbBzJ9fttAstQu1FIntwz4r5nXv8AglzcwQ2W90idnSuaq+VGpjJfK+GejqKI6jSy3drlGus1o3pJwnvSp9TTAhzTjMYsiIwjsM9ylF60m0Q/V7myGktOEkEgHPmiY54S6z21rzyhiJk565zHRoF6+s1kdmK3yzwtHoHvzYhhYuFOOGOa1j9JkhruaDyT0kpkLXUbkSR2BZe1OBdm0aCctcUzn2hTXJfxFPBUAqNHFaXTIAyADxnGkAyOZZ6b4htW23n1NL/hiz+hmkp25wzME79voXNS8Xc3cs9aOEFNhP8Ah6h6KzY7/IqlV4VO/J0abOd01HfxQ3+Fdktbp48nF+Vtzh8GmfbHOyJ2jZuIKirWsuq2duX/ABFE/wAY96zbL+rOy8pTGW2nRA7DgTexPtDnWapUFMsdaqTAWsDXYhgqAjCAMJa70HpWNmvqsrcUnyjejRyjZGWVwz6Oory5XY31QpVFePK7G+qF5B7Ylr8o/GxS3Xy3dVvi5RV+UVLdfLd1W+LlYDi0eb1G+1Rrq1VAC2T5jfEqEVhvVQSIXIePiF6CgPULoMXQoHm9PuQEajtHId1T4K42xuO1vefcivdzsDjLeSdrtx+6gIbs83qH1CkzU5unzOp+ApMFYHS0XAs8ar0M/Es4tHwM5VXoZ+JSuwalCEK5AIQhACEIQAsbev11TrewLZLG3p9dU6ypPolCb5SHRdtD9cPVqr5ZiB9HN2L6p8o5H9m0J/PD1aq+VuYNnxC4dR830OWz5mSeTaYxZjzgJkNGvNotxwDdFmLzrVq1Hnvwx/CsVZ3EExnLY6ZIkd0+Ct3feLqJPkiTP5PWeYDafTproq1WKPZ6Wk0rnQ5Lz+xoOHN8eSsjmuzLopR94jM9EAkbwRvXzqy1C8SSBE9vctJf15ULXT8nWBY9pBg8VzTnGuozORG0xBMpPSsoZhDQYJ1+6S4AtnUzl0rtUYyxk57XJfo8x3dF30q9ItewY25T50HkuPaDkdyWm5a4dh8mekRHTOie2EtstOkKpPlalIVKgyhpc6o5jMtoaRPOVZs99Mgzr/T3rGUot4bM5RreE30Z2pdFr5MGOs3TvXtG6bSzINB5sTYjtKZ2rhA6YptETqc9m7p51UN91/uT0H3qVOPRX/HntlepZqzRD6To24RijuJS62wDk0t6QR4p9S4Q1By6bT1SQe4ypRftJ3KxN6RI9Eqyw/Ehxi/H3ENhuypWpuqNw4WOYwkuAlzzlA2xqenatJdtA032Vpw/8WwYhUc6cOBjuJoMwTOucZBeUa1OoMLSwjEHxE8ZoIBjZkTzqazNaKtma1rYbaaRBA40ue0GTtC1jFItGpLo+lKK8uV2N9RqlUV58s9DPUaqmolr8oqa6+W7qjxcoa/KKmuvlu6o8SrAuXo2X0xvY3xcqtS6ydK9ZvV8j+KkVZvR0Opncwes5eNtXN6VAF9xOcaZDnOeW1a7MToxEMrPY2YAEw0aAJjWrYRkJcTDW/aPsG0nYAoLtsmHiAzjqPdJ2GrUc8jLcXR2KWz03YyXNMxk7ItDZ5Dc8U5AkkCctwiGCF10MJxVHVHPdmSK1VrZ3NYx4DRzR05yum3VSGnlP9at7Xq6QhqAgstlaeTi7aj/AGuKa2EQyoM9DtJ2O3qhZRhOY7kwsJltTo9jkBSufSn1B6iThN7l0pdQeolDVYHoWi4Gcqr0N8Ss6VoeBnLqdVviVK7Bq0IQrkAhCEAIQhACxd5H6ap1ytosXeH1tTruVZ9AUfKR/wAto/rm+rVXyinsmNROZy3zxV9X+Uj/AJbT5qrPCovkmLT4+N64bvm+hzWfMPbkpk43YQ4GGjPdJ2jnC0Ve8qNlb5OnSD69QaNy/bqVI4rJ0yz2DIkI6DH07KKjGlzaeA1ABn9LxhG85hvNIV656OE4qkOqvOJ7tmLLIfdbkANwUKyNEHYl+p8L0we3p9PO+MaeoJJv1b5wF/XZVtNAvqmgXt5IaxzHtnY15ecXO3KekBJuClyVLfUdUFRrKdEho+z5RsOpMI1DZIk5nM7TK0V7WgkOh0FoMCYnnH2jGQA3mdyYfJ06nZ6dVziA20VTVGRIaQGB7ecScuYhdVFkrIKWcyfgu0c+orULJRSxGPn/AN9T57eNZ+NweZcwlh3DCS2BGzJQtf6Qrt+XjSpWuu12B3+IdU4wzLS4uDSRsIcMj6FStd7UntaA1jWtL5IBlxdBAcRGgAA01J2rklXJN5TyeVjPIF0fHxzKOo9dttjHuJpABukTiiRmJdrtK7pWUuMNzJ6NO3pCrGLbwlz5ESxHxPbHSNXEBq1pcd8CJ6TnorL7A0CHCrjbUDXjBxYzDmh4njAjTaJMCFzRuSsJIc1oIgjE6SCYLTAIIMaJhYbwtFJtNgYJa81BJEukEQc9OM7PXPmXXCmUVmxNLzwIzbi9qz5ianYuQcFSHGoCQDnh0wmMzv8AYrfB+0k2iytLnEmvTxAmR9Y0iJ0TkW614Q5tJs03OdPF5Ts9/OdOZK7tu2tTtNmqVWFuO00YMtjN86A5f1V8RwnBt/QVOeeUfY1FefLPQz1GqVQ3qfpD0M9RqHaJq/KKmuvlu6o8Soa+qmuzlu6o8SrAtXvrT6g9ZyxVfhBaASGii0Yg0Eh7iQWCoTAcDIaRsjnW1vbWn+rHrOSundlEfkafKxchvK+1przqrz4AtcGLQ+oGGpGIVXNOEEDiVC3IHMaKDhLY6lQMNMF2EmWgjbEGCRO0dvSrthptpQKbWtDTIaBABmdOlW2uG70n3qU8AhsTHCmwP5QAnbn07Y38ynCkY0bvS73qZlNv2fS73qAKDRq+XxYgaZAyJMtyzEdOc8/Mnd38mp0exy6ZRb9kd596sMYADAiQpbyVjBRzjxE9xni0uo31Epamtw8mj1GeqlQUlj0LQ8DeXU6o8Ss6tDwO5dTqjxUrsGsQhCuQCEIQAhCEALF3h9bU67vFbRYu8franXPiqz6Ao+Usf/y2H/uU/wAa+S06zmklji3LOMsj8DuX2i2MFVgp1QHsEcRwBblpkqAuGy/o1L9wLlsrcnkzlB7spmRuu92fNXU+N5R9RuMnQtawCQd8sHeVwbcA8Z/Gq2guOzDIWennrxV4bhspMmhTnoWdmm3vJ6+l16pr2yWWY2215YTOoK64P3zLRTY2DTBgGJccic9nJA184rZOuSznWiw9/vRZrjs1Myyixp3gEe1VhpseJrd8Qrtjhxz+58+va/iapNMNgxIcwEh2jhJ5xPaqf94aoyw0o/Vt+Ny+kP4M2Rxk2dknM8rXvXP91rH+jt73f7lZ6c8CVCbbSSPmzLxbVeXV6cgNwjyQDCDJIO47dVDdtoLakxMyI3zsX04cFLH+jt38p/8AuXh4IWL8x/HUH4lpTCVU1OOMoiVDlHa8GEZeI4zsJIY0DLzXOeQHSTnkcOm/Ndi2gCjha7FiJni8YEnQEbJ29y3H90LHBHkTBzP0lTP+JB4HWPL6N2Wn0lTLo4y6Z6i+cXF45JrqlXFqPiY2nfVP6Vxa+Biwt4pDXPhuInm4wEbHFSm9hVqWNoBH+Ks7m7iA8Bx7zHYtWOBdj/Nvz1+kfn055ruhwOsjXMe1rwab2vZ9I4gOa4OGR2SBksYuaSXGC8YyRoFXvU/SHoZ6jVYVS9T9IehnqNVzUVVtVNdg456vtVauc1buzlfsnxCsCzeutP8AVj1nKsxWb11p9T8TlWYoBOxTsUDFOxQCdinpqCmp6aAs01MoKasNQCPg/wAmh1WeqEqCacHeTZ+rT9UJW0ZKQdJ/wOP0j+qPFZ8LQcDz9I/qjxVl2DWIQhXIBCEIAQo6tdreU5rZ0kgeK4FrpnSoz94e9ATrE3vUDKzw4hpLiRJiQcwROxbIV2nRze8LsEKGsgwAtDPtt7wu2vG8LQcMLQ2lQ8o/JocMRwkgAggF0DISRn0LD/3gsJMGqyedjh4tWbWCR4hJRe9iP5Sl3R4hSNvGxfnaPaW+1QBuhK22yxnSrQ/fZ71I2vZtlSl2VG+xyAYwvVRaaJ0qDsqf+SkFNmyoeyofegLYQq4swPnP/fd710LIN9T993vQYJl0oBZfvVP3ij5t99/eD7EBIwZZkkn42KRir/Nz9t3c33IdSIa4+UdIE6N5/u8yAtKjeh456Geo1U/nVT847uZ/tS8XI+0vJDrRUJOcOwtHMS0AN9ClIHNutjGuiZcRk1vGcehozV+5nEuzEHBpOmfMn9z8CmME1Drq1k57sdQ8Z2XR0qa/LKym+k2m0NGB+QETmzM7yrNcAUXrrT6n4nKsxWr11p9T8blVYqgnYp2KBinYoBOxTsUDFOxAWKanaq7FYpoBFwcPEs/Vp+qEsCZ8HORZ+rT9UJYrAIT/AIHfWv6n4gkIT7gd9a/qfiClA1yFy94AkkADUnIDtWXvvheKctoM8o4ecSA0dAJl3o6VfJBpbRaG02lz3BrRqSYCzNq4XMecFncJ0xuDv4Wx4rMWuxWm1EvqvxYSeVhhkawARHYO9R3fUpsJGF52yGtggc/lARO3VUbBbtdmNQuq1H4icgXHMcYaScPN2q1QotDoeATkSA6IAOe2T2KQVDyodMCBDhhHY+ObTKNUA5RLs+UYq57chJjdsVSTz6OHOwDcIIIEHPig5ZbSNUeRpS1vk3bC4gg7c+NMaR3r3EMpmGjitiprOpBZs6SufKamRiM5k6Sd5piOfVQCra7O4uGAOYwHMjI65gSM8s8t6gNmr7TUnzW6yJ3HjTGeaYYmaYmgZlxxMl3ScOZ7NF66owO4xYCOSyaY2ZnDi9ykCl1kq6cYnPEYYcOemkaKehYmQccQCQHFogxGoAA3+hWK9rbTEmXOdBloa6BsiHx6Z5gqVpvh0gU2OgbXB8nb5riWiedCCzXu+xh8O8kBmRjpslwAkESMp00KhZdljdp83JMQ0NpyNpJ5tmUbEsr+VrPL3DDOccbMDLLFmTlqdysUaDmMM6yCNJEbyezJASW+5LK0DKm50ThacGughrtfiEndc1GM6TCTMAPrCII14/PuV1tPFm0kuc0lziBiBnQGBl71KbDmWjJsul2FsvEZSenm2qQL/wCwqUhoptmRJx1IzEiJJ+Ny5tVislIN8o8sJE4cUGNpgjTnWisVjhpw4WtwjEQWkNjIk5zJn09q7bdNPPCXNYSQXyDjMaCHTJOzQISZNpsey0VB0PHuUop2fZbLQP2v/BP7TYKbWYn4msiWtJOKoRlsOm8ndklVGxteRjpgCDDSM9DGIbpjVBwcUbM0kBtstJJ2YneOHJPLBdYBa59otIZizxVQQZgGQWwWj3qSxXdTYPKeTYzKOK0NL9+YGkqG11zVcWtIEDOdGg6ZDtUA11kuywtz8oyofvVGkdrRDT2hO6VemAA1zIGgBEDoAXy+ndbfOcT0AD0mUxsdyF3IoE85Ejvdkr5IPoocDoVnuE31tLqVPWpq1wdu19EPx4RiwwAdImZyjaFU4T/W0upU9amj6JQkvXWn1PxuVVnxqrV6a0/1f43KsztVQTMKma5Qt7fQpWzz+hVYLDHjeFMx43hV2k8/8KmYed3cPcgLLKg3hWKTxIzCqscd57lYoOMjXXcgEvBzkWfq0vAJcmXB3kWfq0/AJcpQOSU/4H/Wu6n4mpCPj2LV8F7rqUyaj+LibAbt1Bk7tNOfYrIC69bxFR7mvcQGOc0NjIYSWzrmctVRNOicyR0lq1dq4O2ao4vfT4zsyQ97ZO+GuAVZ3BKzbA8dFR59YlNpAiGGCBUEEEEZjI698lRCw09hbtGp0ORT53A+jsq1h0OZ7WKI8DmbLRW7cB8GhNpItFI7HN7wugx28d4Vx3BB3m2nvpz4OC4PBKtstLT00iP/ALFG0EID9w+Ogr0PqDzfQV67gxaRpUpHpxD2FcHg9bBoaJ/zH+2mo2sEjbQ4HNnikZuoziJBMyZbtmd6bG6LaPMB6Kg9sLj5nbh+Qef8yl/vTDBzZgGNjA0kB2EkCQTJaZjYYShtzjEC5rDnnlmd+zMpuRaxrQq9wd6pKjNorjWhW/0Xn8KnDAtrXa4gDWARtbEmYEbMz3rw3aS6IOCZADiIEbgcyO3RMDeDxyqTh1qTh4tUbr6YOUGDpyUcgoUrBU1OIHDAh5AEuBghpzESmlmoN84uaQcoPMM9N8qNl80j9g9B/qpheFM+b6SoBYbTb51R5DWmGnDB2xyZzXFqIDXVCWuLcOGmWtIJ00iYBjSMlyLXT+ye9dCvS50BnqlldUON7i9xJABEBsQQcPaU4s1mDQHvzyED7UCJPMmFl8k50Eu9GcLmw3c+1vLiS2kDm4amPNZ79ilcgVE1bTU8lRjFtJIAY3eRqegBaW7OBtKkOM97yYLjkMR9g5p7VbuvgxQs9QVKeOQCILsQ42szmU7V1EgqWa7aVPk02g79T3nNW1TvO9aFnbir1mUxsxuDZ5gDmTzBYe+PlYs7Dhs1J9Z2xzvoqfe4Yj+6OlWSB9EWd4Tsd5Sk4Me4BtQHAxz4JNMiQ0GND3L5TfHDm32iQa4ot+xQGEgfefMj94dCy1S11Cc6ryd+J3iSp2ZGT6pft7U2GnMlwaW4YhwcHOMFroI6feFToW979KbR0knwhfNLJbvIvaTOGTMnitxRJcfN0GfozkfUbkttne0EPZ2VGFUccAs0nv8AsNPRI96sMrtHKhp+9A7ipLTbKTGziZ+09oHoWD4ScIM5o1vpJ5VM6N2jENmmSbckm8bbKf5ymP8AMCsU67Do9h/zF8lp8KrUOVaa2WkS6ek4hCYXTfVptNQUqBq1qh83yjm5bS6aha0c5U/hepGT6pTz3H9oq1RGYy2jzio+DPB19NhNsNOq90cTAxzaUahtQtDnztJ7E4/sazfo9H/TZ7lXYDJcHeRZ+rS8Aq9jsT6zsNNs7zo1vWPs1WsZwdoNjAHtjQCo+BGgAJgDmCZWeztptDWNDQNg8ec86KIKF03Iyjxjxn/aOzqjzfFNEIVwCEIQAhCEAIQhACEIQAhCEAIQhACEIQEdSg13Ka09IB8VWfdFnOtCkemmw+xCEBEbgsv6PSHQxrfAKN3BuzH8lHQ948HIQgOWcGrMDIa8Ef8Adq+1ya0qYaA1oAAEADQBCEB2hCEBiLb8l9iqPLy60YjqTVLyel1QOce0qlU+SKyebXtA6TTP4AvEKcsHDfknptg07U4QSZNJjjmIzIIy5lHX+S+qQQLayNxs7Y9dCE3MjAttHyS2k6Wqj/pYfAFUz8lltp/lrOQPvVGn0UyPQhCZJFlt4EVmmahYSNoqk+NFLRcZxYYz/WZfykIVkxg19wfJeazcdWo1gPJDSahO+SWsw+lbL+4dF1NtF7h5NpkNZSothw87GWF4d94OnnXqFXIwatjYAA2CMzJy3k6rpCFABCEIAQhCA//Z",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRvOwoVt9XJexsbN76-D2qaeUIXVu9rhZeRjQ&usqp=CAU", 
    "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEA8QEA0NDQ8NDQ0NDw8NDQ8NDQ8NFREWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zOD8sNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIALcBEwMBIgACEQEDEQH/xAAYAAEBAQEBAAAAAAAAAAAAAAAAAQIDB//EACQQAQEBAQACAwACAgMBAAAAAAABAhExcQMhQRKxgfFRYZET/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/APYgAAcPk336ngE+X5O/U8f2mMLjDvmAmctAABmdAkav01ZxztAAAAABjVA1VzDMaAAAAAAAAATVLWZO+gJO+mwAJFkaBOCfyUGQct66Cb130mctZy6SASKAALnPQTOeu3JmL9Zn91x1roGqgAAAAxqgaq5yZy3ICQrVZAAAQUAABLS1mToEnWwAamTOXTwDPhjV6a11AAAc9XpnLUy0CSKAAN5zz3/wCZz/AOunhO8c9aA1rrIoAsjUgM8SraxaBqrmEjWc9AzOtNMaoJagAInloAABLSpzoJJ1sAG85M5dPAJ4cta6a11AAAAAAAAdMzn3fP8AQGc8+75/oumdaZAtZ8nloBZlc5dJASRz1pd676YoFpIsnGsY6BjPXTw14c9UE1WC0AZ8nlQUFkBAqAjQAN5yZy6ePsDx91x3rq711kAAAAAAAFgNZnPv9Zt6loAz5Tz6bAazkzl2zkCZcvk330fJvvpgBqTiycXGP5ev2gmMd9O3hfH1HPWgTWnO01UAZ8l+1kAii5z0DOerqrrX5GAAAG85TMdPAL48uO9dNa6gAAAigA1IDI10BkABjz6Xz6aAazkzHbMAzly+T5O+PH9nyfJ30xIBI6ScWThnPfQJjH8vX7Xfx9Q8Tkc9aA1px1V1WQGfJ5WASKNYx30BjHfTW9fkXe+fUcgAAFzCRvwC+HPWulvQEAAS1LVkAiyLI6ZyDMymqu9fkYAAAZ8qoC5iSOuYC5jHyb76Te++mZAJHWTntZOe0k6BJ306+E8Ma0BrTnqmqyAz5PLQAN/H8ffQHx476a+TfPqL8m+fU/04gAAEGoCxPJxbeAl+mQAZt/4LVkAkakMx2xkEzln5N/kX5N/k/wA1yAAAAABYDWYmtdS1JAJHbM57MzntPIHlpGboF1pztLUAQUAG/jx30B8fx99N/Jvn1P8AR8nyc+p/pxAAAAAjchnLWrz2CW89uYAM2lqyASNZhmO2MgYyz8vyfk/zT5Pk/J/muQALnPQSQq6qAAAFoSASOuZwk4zaC2idS0FtYtABFAAa+PHfQL8eO+m/k3z6ifJvn1HIAAAABvOVzlrWuewTV57ci0AZtW0kAkakSR1xkFxlPk+T8n+am9/kcwBZG85BnOOmtfkXevyMAAAC/wAVBmOk+kn0zaC2p1AF6gAAAA1jPfQGMd9N73z6ia3+RzAAAAAdM5TOWta57Bda57cbSgCUUEkUazAXMN6/ImtMgLnK4y6X6+v0EkZ3r8n+01pkAABrMSQtBr/6f9DAC2oAAAAAALIBnLWtfkS6ZAAAAAbzlMxq657Bda57cqAAAAAEW1ABvGUxl01r+Pv+gNa59fv9OVqAAAAAAAAAAAAAAAC9AEAAAAWRQGreOYAAAAAAALmKA3rXPbkAAAAAAAAAAAP/2Q=="]
// Global variables accessible for all functions
let payBalance = 0
let bankBalance = 0
let laptops = []

/* called when work-btn is pressed. 
*  adds 100 kr to the pay balance
*/
function work() {
    payBalance+=100
    PAYBALANCETXT.innerHTML = payBalance + " Kr";
    // console.log(laptops);
}

/**
 * called when the bank-btn is pressed.
 * Does not bank if the pay balance is 0
 * adds the pay balance to the bank
 */
function bank() {
    if(payBalance == 0) {
        alert("You have nothing to bank!")
    } else {
        bankBalance+= payBalance;
        BANKBALANCETXT.innerHTML = bankBalance + "Kr"
        payBalance=0;
        PAYBALANCETXT.innerHTML = payBalance + " Kr" 
    }
}

/**
 * called when the get a loan-btn is pressed.
 * Promts the user if user have nothing in bank balance.
 * Lets user loan money up to double the amount in bank.
 */
function getLoan() {
    if(bankBalance == 0) {
        alert("You have no balance")
    } else {
        let amount = window.prompt("how much would you like to loan?")
        if(Number(amount) > bankBalance*2) {
            alert("You cannot loan that much, try again")
        } else {
            bankBalance+= Number(amount)        
            BANKBALANCETXT.innerHTML = bankBalance + " Kr";
            document.getElementById("btn-get-loan").disabled = true
        }
    }
}

/**
 * called when the buy-btn is pressed.
 * Calculates to check if the user has enough money
 * in the bank to buy the selected & displayed item.
 * Prompts "congrats" and subtracts the amount on bank
 * if user was able to buy, otherwise tells the user to
 * keep working
 */
function buy() {
    let price = PRICE.innerHTML
    price = price.substring(0, price.lastIndexOf(' '))
    console.log(price);
    if(bankBalance>=Number(price)) {
        bankBalance-= price
        BANKBALANCETXT.innerHTML = bankBalance + " Kr"
        document.getElementById("btn-get-loan").disabled = false
        alert("Congratulations, you have bought a new laptop!")
    } else {
        alert("You do not have enough money. Keep working!")
    }
     
}

/**
 * Creates an array with Laptop-objects. Hardcoded to be 4 objects.
 */
function populateArr() {
    const features = [
        ["It has a screen", "There's also a keyboard", "(Some keys might be missing)"], 
    ["It has a screen, keyboard AND a mouse!", "It might have some chips crumbles between the keys", "(Some keys might be missing)"], 
    ["Very cheap!", "Extremely cheap!", "The cheapest option of them all!"], 
    ["It is screenless", "It is invisible", "It is PAPPLE PRODUCT!"] ]
    const descriptions = ["This laptop will turn on! It is perfect for first time users who likes to have a Komputer around!",
    "This laptop work just as intended. It has been used by famous people, so deal with the price",
    "This laptop is cheap! How nice isn't that?!", "This laptop is made by Papple and is invisible. To log in, just imagine it in your head!"]
    const prices = [3000, 1999, 800, 15000]
    const names = ["Intol Celeroo E10", "Intol Celeroo E20", "Intol Celeroo E1", "Papple pc"]

    for(let i=0; i<4;i++) {        
        laptops[i] = new Laptop(names[i], prices[i], features[i], descriptions[i])
    }
    // console.log(laptops);
}

/**
 * Called on-load.
 */
function initPage() {
    populateArr()
    createOptions()
    displayInfo()
}

/**
 Fills the select options.
 */
function createOptions() {
    let option
    for(let i=0; i<4; i++) {
        option = document.createElement("option")
        option.textContent =  laptops[i].name
        // console.log(laptops[i].name);
        LAPTOPSELECTOR.appendChild(option)
    }   
}

/**
 * Displays info depending on what item is selected.
 */
function displayInfo() {
    let feature
    let index
    FEATURESLIST.innerHTML =""
    for(let i=0; i<4; i++) {
        if(LAPTOPSELECTOR.value == laptops[i].name) {
            index = i
            for(let j=0; j<3; j++) {
                feature = document.createElement("li")
                feature.textContent = laptops[i].features[j]
                // console.log(laptops[i].features[j]);
                FEATURESLIST.appendChild(feature)
            }
        }
    }
    // set all of the information
    LAPTOPNAME.innerHTML = laptops[index].name
    DESCRIPTION.innerHTML = laptops[index].desc
    PRICE.innerHTML = laptops[index].price + " KR"
    image.src = imageURLs[index]
}


class Laptop {
    constructor(name, price, features, desc) {     
        this.name = name   
        this.desc = desc
        this.price = price
        this.features = features
    }

    getName() {
        return $this.name
    }

    getDescription() {
        return $this.desc
    }

    getPrice() {
        return $this.price
    }

    getFeatures() {
        return $this.features
    }
}